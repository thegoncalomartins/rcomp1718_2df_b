

const TIMEOUT_VAL = 1500; // millisseconds (1.5 seconds)

const ERR_TIMEOUT_VAL = 1000; // millisseconds (1 second)

const TRYING_MSG = "No messages to show.";

const TIMEOUT_VAL1 = 5000; // 5 seconds



function executeOperation(uri, op, req) {

  req.open(op, uri, true);

  req.timeout = TIMEOUT_VAL1;

  req.send();

}



function openWall() {

  var wallName = document.getElementById("wall-name").value;



  if (wallName.includes("/")) {

    alert("Wall's name can't include '/' character!");

    return;

  }



  if (!wallName.trim()) {

    alert("Wall's name can't be empty!");

  } else {

    document.getElementById("message-send").disabled = false;

    document.getElementById("wall-remove").disabled = false;

    document.getElementById("wall-name-h1").innerHTML = "Current Wall: ";

    document.getElementById("wall-name-h2").innerHTML = decodeURI(wallName);

    document.getElementById("wall-name").value = null;

    document.getElementById("message-clear").disabled = false;

    listMessages();

  }

}



function listMessages() {

  var req = new XMLHttpRequest();

  var wallName = encodeURI(document.getElementById("wall-name-h2").innerHTML);

  var uri = "/walls/".concat(wallName);

  var elementId = "message-container";

  var op = "GET";



  req.onload = function upDate() {



  document.getElementById(elementId).innerHTML = decodeURI(req.responseText);



    setTimeout(listMessages, TIMEOUT_VAL);

  };



  req.ontimeout = function timeoutCase() {

    document.getElementById(elementId).innerHTML = TRYING_MSG;

    setTimeout(listMessages, ERR_TIMEOUT_VAL);

  };



  req.onerror = function errorCase() {

    document.getElementById(elementId).innerHTML = TRYING_MSG;

    setTimeout(listMessages, ERR_TIMEOUT_VAL);

  };



  executeOperation(uri, op, req);

}



function addMessage() {

  var req = new XMLHttpRequest();

  var msg = document.getElementById("message").value;

  var wallName = encodeURI(document.getElementById("wall-name-h2").innerHTML);

  var uri = "/walls/".concat(wallName).concat("/").concat(msg);

  var op = "POST";

  var elementId = "messages";

  document.getElementById("message").value = null;

  if (!msg.trim()) {

    alert("Message can't be empty!");

  } else {

    executeOperation(uri, op, req);

  }



}



function deleteMessage(msgNumber) {

  var req = new XMLHttpRequest();

  var wallName = encodeURI(document.getElementById("wall-name-h2").innerHTML);

  var uri = "/walls/".concat(wallName).concat("/").concat(String(msgNumber));

  var op = "DELETE";



  executeOperation(uri, op, req);

}



function deleteWall() {

  var req = new XMLHttpRequest();

  var wallName = encodeURI(document.getElementById("wall-name-h2").innerHTML);

  var uri = "/walls/".concat(wallName);

  var op = "DELETE";

  document.getElementById("wall-name-h1").innerHTML = "Your wall's name will appear here!";

  document.getElementById("wall-name-h2").innerHTML = null;

  document.getElementById("message-send").disabled = true;

  document.getElementById("wall-remove").disabled = true;

  document.getElementById("message-clear").disabled = true;



  executeOperation(uri, op, req);

}



function clearMsgTextArea() {

  document.getElementById("message").value = null;

}
