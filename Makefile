# variaveis
INCLUDES_HTTP = http.h msg_wall.h
SOURCES_HTTP = main.c http.c msg_wall.c
OBJFILES_HTTP = main.o http.o msg_wall.o
SOURCES_UDP = udp_cli.c
OBJFILES_UDP = udp_cli.o
SERVER = server
CLIENT = client

# regras de sufixo
.SUFFIXES : .c .o

# como transformar um .c num .o ; $< -- nome do ficheiro
.c.o:
	gcc -Wall -g -c $<

all: ${SERVER} ${CLIENT}

${SERVER}: ${OBJFILES_HTTP}
	gcc -Wall -g -o ${SERVER} ${OBJFILES_HTTP} -lpthread

${CLIENT}: ${OBJFILES_UDP}
	gcc -Wall -g -o ${CLIENT} ${OBJFILES_UDP} 

${OBJFILES_HTTP}: ${SOURCES_HTTP} ${INCLUDES_HTTP}

${OBJFILES_UDP}: ${SOURCES_UDP}

run:  ${SERVER}
	./${SERVER}

clean:
	rm -f ${OBJFILES_HTTP}
