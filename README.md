# README #

* Computer Networks Course ISEP
* Computer Engineering Bachelor Degree
* Gonçalo Martins, 1161544
* João Dias, 1160967
* Luísa Ramos, 1161010
* Miguel Castro, 1160574
* 2DF, 2017/2018.

## What is this repository for? ##

* Computer Networks Project #2
* Version 1.0

## Proposal ##
This project aims at implementing a walls service, each wall is a board  
where  users  can  freely  add  plain  text  only  messages.  The  project  is   
designed for a friendly working environment without security concerns.   
No  user authentication  or  authorization  is  enforced,  to  add  a  message,   
the  only  requirement  is  knowing  the  wall’s  name.  However, walls’  names   
are not publicly available, so they can be also regarded as access keys.   
Messages in a wall are publicly available and may be removed by any user,   
again, as far at the wall’s name is known.   
Two  applications  are  to  be  developed,  the  main  one  is  the server,  
it manages the walls and it’s in essence an HTTP server, it provides a web   
users’  frontend,  REST  web  services  and  also  a  low  level  UDP  messages interface.  
The  second  application  is  a  simple UDP  client that  allows  the  direct posting  of   
messages  in  a  wall  by  using  the  mentioned  low  level  UDP messages interface.   
Applications may be developed either in C or Java language, regarding the   
web frontend, JavaScript will also be required.

## Project Use Cases ##
* View  text  messages  in  a  named  wall.  As  new  messages  are  added  or removed the displayed messages must be automatically updated. 

* Add  a  text  message  to  a  named  wall,  if  the  wall  doesn’t  exist, it  is created. 

* Remove a text message from a named wall. 

* Remove a wall (and all messages within). 

## How do I get set up? ##

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Contribution guidelines ##

The main application (server) in an HTTP server, it must be implemented  
from scratch, this is a requirement for the project. No already available   
HTTP  server  implementations  can  be  used.  Nevertheless,  code  parts  of   
publicly and freely available HTTP server implementations are acceptable,   
as far as sources are quoted.   
The  HTTP  server  (included  in  the  server  application)  is  required  to   
implement methods GET, POST and DELETE, be able to fetch static contents   
from  files  within  a  base  folder  (GET),  and  provide  the  appropriate   
Content-type header field for some relevant content types, for instance:   
text/plain,  text/html,  image/gif,  image/png,  application/javascript,   
application/xml. For file contents, the content type may be determined by   
the file’s name or extension.   
### 1. Web main page loading (users’ frontend) ###
The HTTP server main web page (GET /), usually index.html is loaded only  
once,  no  form  submissions  are  to  be  used,  it  must  use  JavaScript  to   
implement  all  use  cases,  acting  as  a  web  services  consumer  (AJAX),  
namely:   
* Add a message to the current wall. 
* Remove a message from the current wall. 
* Remove the current wall
### 2. REST Web Services ###
The server application manages walls, each wall is identified by a name,   
within each wall, and text messages are identified by a sequence number   
settled  by  the  server  application.  Within  each  wall,  the  first  posted   
message  will  have  number  zero,  following  posted  messages  will  have   
numbers incremented by one.   
The server application’s data is non persistent, if the server is stopped  
or restarted all messages and walls are lost.   
The web services REST API should be implemented by using the following   
URI structure:   
/walls/{WALL’S-NAME}/{MESSAGE-NUMBER}   
Where {WALL’S-NAME} stands for the wall’s name and {MESSAGE-NUMBER}   
stands for the message’s sequence number.   
REST use cases:   
* Add a message: POST /walls/{WALL’S-NAME}   
* Delete a message: DELETE /walls/{WALL’S-NAME}/{MESSAGE-NUMBER}   
* Delete a wall: DELETE /walls/{WALL’S-NAME}   
* List messages: GET /walls/{WALL’S-NAME}   
GET /walls shouldn’t be allowed by the server because wall names are not   
supposed to be publicly available.   
The  PUT  method  is  not  to  be  implemented,  messages  can’t  be  updated.   
Deleting a wall removes all wall’s messages  
Walls  are  created  on  demand,  meaning  a  message  POST  to  a  non-existing   
wall name implies the wall is automatically created  
### 3. Low level UDP messages interface ###
In addition to the HTTP service, the server application also provides a   
UDP interface. This service is to be made available on a UDP port number   
equal to the TCP port number being used by the HTTP service. The only use   
case available through UDP is adding a message to a wall, equivalent to a  
POST through the web services REST interface.   
For  this  purpose  an  application  protocol  specification  must  be   
established, namely regarding the structure of UDP requests and replies.   
A UDP client application must then be implemented to send requests to the   
server, and thus add a text message to a specified wall name.   
The  UDP  client  should  be  able  to  send  the  request  either  to  a  unicast   
server’s address or to the local IPv4 broadcast address.  
### 4. Applications configuration data ###
Both applications (server and UDP client) require some configuration data   
to  run,  the  most  straightforward  way  to  provide  it  is  through  command   
line arguments:   
Server application:    
* The local port number (the same number is used both for the UDP 
service and for the TCP/HTTP service).
* The base folder for static contents files. 
UDP client application: 
* The server’s port number. 
* The server’s IP address/DNS name or IPv4 broadcast. 
* The wall’s name and the message’s text. 

## Who do I talk to? ##

* Gonçalo Martins, 1161544@isep.ipp.pt