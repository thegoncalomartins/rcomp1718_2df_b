#ifndef __RCOMP_HTTP_H
#define __RCOMP_HTTP_H

#include <pthread.h>
#include "msg_wall.h"

#define HTTP_VERSION "HTTP/1.0"

#define HTTP_CONNECTION_CLOSE "Connection: close"

#define BASE_FOLDER "www"

#define MESSAGE_HTTP_LINE_LIMIT 1024
#define GET_LINE_LIMIT 256
#define POST_LINE_LIMIT 512
#define DELTE_LINE_LIMIT 256
#define WALL_NAME_LIMIT 100

void readlineCRLF(int sock, char *line);
void writelineCRLF(int sock, char *line);

void sendHttpResponseHeader(int sock, char *status, char *contentType,
		int contentLength);
int sendHttpResponse(int sock, char *status, char *contentType, char *content,
		int contentLength);
void sendHttpStringResponse(int sock, char *status, char *contentType,
		char *content);
void sendHttpFileResponse(int sock, char *status, char *filename);

void list_messages(int sock, char *wall_name, walls_t *walls,
		pthread_mutex_t *mux);
void process_get(int sock, char *request_line, walls_t *walls,
		pthread_mutex_t *mux);
void process_post(int sock, char *request_line, walls_t *walls,
		pthread_mutex_t *mux);
void process_delete(int sock, char *request_line, walls_t *walls,
		pthread_mutex_t *mux);
void process_http_request(int sock, int con_sock, walls_t *walls,
		pthread_mutex_t *mux);

#endif
