#include <strings.h> 
#include <string.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <stdio.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <netdb.h> 

#define BUF_SIZE 300

// read a string from stdin protecting buffer overflow
#define GETS(B,S) {fgets(B,S-2,stdin);B[strlen(B)-1]=0;}

int barra_wall_name(char* wall) {
  int i = 0;
  while (*(wall+i) != 0) {
    if(*(wall+i) == '/') {
      return 0;
    }
    i++;
  }
  return 1;
}

int main(int argc, char **argv) {//server port = argv[1]; ip = argv[2]; wall name = argv[3]; message = argv[4]
  struct sockaddr_storage serverAddr;
  int sock, res, err;
  unsigned int serverAddrLen;
  struct addrinfo  req, *list;
  char message[BUF_SIZE];
  
  if(argc!=5) {
    puts("Server Port, IPv4 address, wall name and message are all required as argument. (in this order)");
    exit(1);
  }

  if (barra_wall_name(argv[3]) == 0) { // verifica se a wall name tem barra
      puts("Wall Name can't contain '/'");
      exit(1);
    }
    
  bzero((char *)&req,sizeof(req));
  req.ai_family = AF_UNSPEC;		// let getaddrinfo set the family depending on the supplied server address
  req.ai_socktype = SOCK_DGRAM;
  err=getaddrinfo(argv[2], argv[1] , &req, &list);
  
  if(err) {
    printf("Failed to get server address, error: %s\n",gai_strerror(err)); exit(1); }
  serverAddrLen=list->ai_addrlen;
  memcpy(&serverAddr,list->ai_addr,serverAddrLen);  // store the server address for later use when sending requests
  freeaddrinfo(list);
  
  bzero((char *)&req,sizeof(req));
  
  // for the local address, request the same family asdetermined for the server address
  req.ai_family = serverAddr.ss_family;
  req.ai_socktype = SOCK_DGRAM;
  req.ai_flags = AI_PASSIVE;			// local address
  err=getaddrinfo(NULL, "0" , &req, &list);	// port 0 = auto assign
  if(err) {
    printf("Failed to get local address, error: %s\n",gai_strerror(err)); exit(1); }
  
  sock=socket(list->ai_family,list->ai_socktype,list->ai_protocol);
  if(sock==-1) {
    perror("Failed to open socket"); freeaddrinfo(list); exit(1);}
  if(bind(sock,(struct sockaddr *)list->ai_addr, list->ai_addrlen)==-1) { 
    perror("Failed to bind socket");close(sock);freeaddrinfo(list);exit(1);}
    
  freeaddrinfo(list);

  sprintf(message, "%s/%s", argv[3], argv[4]);
  sendto(sock,message,strlen(message),0,(struct sockaddr *)&serverAddr,serverAddrLen);
  res=recvfrom(sock,message,BUF_SIZE,0,(struct sockaddr *)&serverAddr,&serverAddrLen);
  message[res]=0; /* NULL terminate the string */
  printf("Received reply: %s\n",message);
  close(sock);
  exit(0);
}
