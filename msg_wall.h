#ifndef __RCOMP_MSG_WALL_H
#define __RCOMP_MSG_WALL_H

/**
 * A wall.
 */
typedef struct {
  char* name; // wall's name
  char** msgs; // wall's messages
  int len; // msgs array length
} wall_t;

/**
 * The list of walls.
 */
typedef struct {
  wall_t* msg_walls;
  int size; // size of the list of walls
} walls_t;

/**
 * Creates a new list of walls and returns a pointer to it.
 */
walls_t* create_walls_list();

/**
 * Adds a new wall to the list of walls.
 * @param walls - list of walls
 * @param wall_name - the wall's name
 * @return a pointer to the created wall
 */
wall_t* add_wall(walls_t* walls, char* wall_name);

/**
 * Returns the index of the wall with the name received by parameter
 * or minus one if it doesn't exist.
 *
 * @param walls - list of walls
 * @param wall_name - the wall's name
 * @return the index of the wall, -1 if it doesn't exist
 */
int find_wall(walls_t* walls, char *wall_name);

/**
 * Shifts every wall from the received walls list one position to the left.
 * @param walls - the walls
 * @param index - the position from which the shift should be performed
 */
void shift_walls(walls_t* walls, int index);

/**
 * Removes a wall from the list of walls.
 * @return true if the wall existed, false otherwise.
 */
int remove_wall(walls_t* walls, char *wall_name);

/**
 * Adds a new message to the specified wall.
 * @param wall - the wall
 * @param msg - the message
 */
void add_message(wall_t* wall, char* msg);

/**
 * Shifts every message from the received wall one position to the left.
 * @param wall - the wall
 * @param index - the position from which the shift should be performed
 */
void shift_messages(wall_t* wall, int index);

/**
 * Removes a message from the list of messages of the specified wall.
 * @param wall - the wall
 * @param msg_index - the message index
 * @return true if the message existed, false otherwise.
 */
void remove_message(wall_t* wall, int msg_index);

#endif
