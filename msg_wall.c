#include "msg_wall.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

walls_t* create_walls_list() {
  walls_t* walls = (walls_t*) malloc(sizeof(walls_t));
  walls->msg_walls = (wall_t*) malloc(sizeof(wall_t));
  walls->size = 0;
  return walls;
}

wall_t* add_wall(walls_t* walls, char* wall_name) {
  int size1;
  wall_t* new_wall = (wall_t*) malloc(sizeof(wall_t));
  new_wall->name = strdup(wall_name);
  new_wall->msgs = (char**) malloc(sizeof(char*));
  new_wall->len = 0;
  walls->msg_walls = (wall_t*) realloc(walls->msg_walls, ++walls->size * sizeof(wall_t));
  size1 = walls->size;
  *(walls->msg_walls + --size1) = *new_wall;
  printf("wall '%s' created\n", wall_name);
  return new_wall;
}

int find_wall(walls_t* walls, char *wall_name) {
	int number_of_walls = walls->size;
	char *name;
	int i;

	for (i = 0; i < number_of_walls; i++) {
		name = (walls->msg_walls+i)->name;
		if (strcmp(wall_name, name) == 0) {
			return i;
		}
	}

	return -1;
}

void shift_walls(walls_t* walls, int index) {
	int size1 = walls->size;
	int i;
	for(i = index; i < size1 - 1; i++) {
		*(walls->msg_walls + i) = *(walls->msg_walls + (i + 1));
	}
}

int remove_wall(walls_t* walls, char *wall_name) {
	int pos = find_wall(walls, wall_name);
	if(pos != -1) {
		/* shift every wall 1 position to the left */
		shift_walls(walls, pos);
		walls->msg_walls = (wall_t*) realloc(walls->msg_walls, --walls->size * sizeof(wall_t));
		printf("wall '%s' removed\n", wall_name);
		return 1;
	}
  return 0;
}

void add_message(wall_t* wall, char* msg) {
  int size;
  wall->msgs = (char**) realloc(wall->msgs, ++wall->len * sizeof(char*));
  size = wall->len;
  *(wall->msgs + --size) = strdup(msg);
}

void shift_messages(wall_t* wall, int index) {
	int size1 = wall->len;
	int i;
	for(i = index; i < size1 - 1; i++) {
		*(wall->msgs + i) = *(wall->msgs + (i + 1));
	}
}

void remove_message(wall_t* wall, int msg_index) {
	/* shift every message 1 position to the left */
	shift_messages(wall, msg_index);
	wall->msgs = (char**) realloc(wall->msgs, --wall->len * sizeof(char*));
}
