#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "http.h"

void readlineCRLF(int socket, char *line) {
	char* aux = line;
	for (;;) {
		read(socket, aux, 1);
		if (*aux == '\n') {
			*aux = 0;
			return;
		} else if (*aux != '\r') {
			aux++;
		}
	}
}

void writelineCRLF(int socket, char *line) {
	write(socket, line, strlen(line));
	write(socket, "\r\n", 2);
}

void sendHttpResponseHeader(int sock, char *status, char *contentType,
		int contentLength) {
	char aux[200];
	sprintf(aux, "%s %s", HTTP_VERSION, status);
	writelineCRLF(sock, aux);
	sprintf(aux, "Content-type: %s", contentType);
	writelineCRLF(sock, aux);
	sprintf(aux, "Content-length: %d", contentLength);
	writelineCRLF(sock, aux);
	writelineCRLF(sock, HTTP_CONNECTION_CLOSE);
	writelineCRLF(sock, "");
}

int sendHttpResponse(int sock, char *status, char *contentType, char *content,
		int contentLength) {
	int done, todo;
	char *aux;
	sendHttpResponseHeader(sock, status, contentType, contentLength);
	aux = content;
	todo = contentLength;
	while (todo) {
		done = write(sock, aux, todo);
		if (done < 1)
			return (0);
		todo = todo - done;
		aux = aux + done;
	}
	return (1);
}

void sendHttpStringResponse(int sock, char *status, char *contentType,
		char *content) {
	sendHttpResponse(sock, status, contentType, content, strlen(content));
}

void sendHttpFileResponse(int sock, char *status, char *filename) {
	FILE *f;
	char *aux;
	char line[200];
	int done;
	long len;
	char *contentType = "text/html";

	f = fopen(filename, "r");
	if (!f) {
		sendHttpStringResponse(sock, "404 Not Found", contentType,
				"<html><body><h1>404 File not found</h1></body></html>");
		return;
	}
	aux = filename + strlen(filename) - 1;
	while (*aux != '.' && aux != filename)
		aux--;
	if (*aux == '.') {
		if (!strcmp(aux, ".pdf"))
			contentType = "application/pdf";
		else if (!strcmp(aux, ".js"))
			contentType = "application/javascript";
		else if (!strcmp(aux, ".txt"))
			contentType = "text/plain";
		else if (!strcmp(aux, ".gif"))
			contentType = "image/gif";
		else if (!strcmp(aux, ".png"))
			contentType = "image/png";
		else if (!strcmp(aux, ".css"))
			contentType = "text/css";
	} else
		contentType = "application/x-binary";

	fseek(f, 0, SEEK_END);
	len = ftell(f);
	if (!status)
		status = "200 Ok";
	sendHttpResponseHeader(sock, status, contentType, len);
	rewind(f);
	do {
		done = fread(line, 1, 200, f);
		if (done > 0)
			write(sock, line, done);
	} while (done >= 0);
	fclose(f);
}

void list_messages(int sock, char *wall_name, walls_t *walls,
		pthread_mutex_t* mux) {
	int index = find_wall(walls, wall_name);
	wall_t *wall;
	int n = 4;
	if (index != -1) {
		wall = walls->msg_walls + index;
	} else {
		return;
	}

	pthread_mutex_lock(mux);
	//	the wall's messages
	char **msgs = wall->msgs;
	int number_of_messages = wall->len;
	pthread_mutex_unlock(mux);
	char* buffer = (char*) malloc(n * sizeof(char));
	char line[512];
	strcpy(buffer, "<ol>");
	int i;
	for (i = 0; i < number_of_messages; i++) {
		n +=
				sprintf(line,
						"<li>%s<span class=\"my-class\"><button id=\"delete%d\" onclick=\"deleteMessage(%i)\" type=\"button\" class=\"btn btn-primary\"> <i class=\"fa fa-trash\"></i> Remove</button></span></li><hr>",
						*(msgs + i), i + 1, i);
		buffer = (char*) realloc(buffer, n * sizeof(char));
		strcat(buffer, line);
	}


	n += sprintf(line, "</ol>");
	buffer = (char*) realloc(buffer, (n + 1) * sizeof(char));
	strcat(buffer, line);

	sendHttpStringResponse(sock, "200 Ok", "text/html", buffer);
}

void process_get(int sock, char *request_line, walls_t *walls,
		pthread_mutex_t* mux) {
	char line[GET_LINE_LIMIT];

	do {
		readlineCRLF(sock, line);
	} while (*line);

	char uri[GET_LINE_LIMIT];
	strcpy(uri, request_line + 4);

	char *aux;
	aux = uri;
	while (*aux != 32) {
		aux++;
	}
	*aux = 0;

	if (strncmp(uri, "/walls", 6) == 0) {
		list_messages(sock, uri + 7, walls, mux);
		return;
	}
	if (!strcmp(uri, "/")) {
		strcpy(uri, "/wall.html"); // BASE URI
	}

	char filepath[GET_LINE_LIMIT];
	strcpy(filepath, BASE_FOLDER);
	strcat(filepath, uri);
	sendHttpFileResponse(sock, NULL, filepath);
}

void process_post(int sock, char *request_line, walls_t *walls,
		pthread_mutex_t* mux) {
	char line[POST_LINE_LIMIT];

	do {
		readlineCRLF(sock, line);
	} while (*line);

	char uri[POST_LINE_LIMIT];
	strcpy(uri, request_line + 5);

	char *aux;
	aux = uri;
	while (*aux != 32) {
		aux++;
	}
	*aux = 0;

	aux = uri;
	aux += 7;

	int i = 0;
	char wall_name[WALL_NAME_LIMIT];
	while (*aux != '/') {
		wall_name[i] = *aux;
		i++;
		aux++;
	}
	wall_name[i] = 0;
	aux++;

	pthread_mutex_lock(mux);
	int index = find_wall(walls, wall_name);
	wall_t* wall;
	if (index == -1) {
		add_wall(walls, wall_name);
		index = find_wall(walls, wall_name);
	}
	wall = walls->msg_walls + index;
	add_message(wall, aux);
	pthread_mutex_unlock(mux);
}

void process_delete(int sock, char *request_line, walls_t *walls,
		pthread_mutex_t* mux) {
	char line[DELTE_LINE_LIMIT];

	do {
		readlineCRLF(sock, line);
	} while (*line);

	char uri[DELTE_LINE_LIMIT];
	strcpy(uri, request_line + 7);

	char *aux;
	aux = uri;
	while (*aux != 32) {
		aux++;
	}
	*aux = 0;

	aux = uri;
	aux += 7;

	int i = 0;
	char wall_name[WALL_NAME_LIMIT];
	while (*aux != 0) {
		if (*aux == '/') {
			wall_name[i] = 0;
			aux++;
			pthread_mutex_lock(mux);
			remove_message(walls->msg_walls + find_wall(walls, wall_name),
					atoi(aux));
			pthread_mutex_unlock(mux);
			return;
		}
		wall_name[i] = *aux;
		i++;
		aux++;
	}
	wall_name[i] = 0;
	pthread_mutex_lock(mux);
	remove_wall(walls, wall_name);
	pthread_mutex_unlock(mux);
}

void process_http_request(int sock, int con_sock, walls_t *walls,
		pthread_mutex_t* mux) {
	char request_line[200];

	readlineCRLF(con_sock, request_line);
	printf("%s\n", request_line);

	if (strncmp(request_line, "GET /", 5) == 0) {

		pid_t p = fork();
		if (p == 0) {
			close(sock);
			process_get(con_sock, request_line, walls, mux);
			close(con_sock);
			exit(0);
		}
	} else if (strncmp(request_line, "POST /", 6) == 0) {
		process_post(con_sock, request_line, walls, mux);
	} else if (strncmp(request_line, "DELETE /", 8) == 0) {
		process_delete(con_sock, request_line, walls, mux);
	} else {
		sendHttpStringResponse(con_sock, "405 Method Not Allowed", "text/html",
				"<html><body>HTTP method not supported</body></html>");
		perror("method not supported by the server");
	}

	close(con_sock);
}
