#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <pthread.h>

#include "http.h"
#include "msg_wall.h"

#define BASE_FOLDER "www"

pthread_mutex_t mux = PTHREAD_MUTEX_INITIALIZER;

typedef struct {
	walls_t* walls;
	char* port;
	pthread_mutex_t* mux;
} arg;

int prepare_connection(char *port, struct addrinfo *res) {

	struct addrinfo hints;
	bzero((char*) &hints, sizeof(hints));

	hints.ai_family = AF_INET6;	//	requesting a IPv6 local address will allow both IPv4 and IPv6 clients to use it
	hints.ai_socktype = SOCK_STREAM;	//	SOCK_STREAM for TCP
	hints.ai_flags = AI_PASSIVE;		// local address

	if (getaddrinfo(NULL, port, &hints, &res) != 0) {
		perror("failed to get local address");
		exit(EXIT_FAILURE);
	}

	int sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (sockfd == -1) {
		perror("failed to open socket");
		freeaddrinfo(res);
		exit(EXIT_FAILURE);
	}

	if (bind(sockfd, (struct sockaddr*) res->ai_addr, res->ai_addrlen) == -1) {
		perror("bind failed");
		close(sockfd);
		freeaddrinfo(res);
		exit(EXIT_FAILURE);
	}

	freeaddrinfo(res);
	listen(sockfd, SOMAXCONN);

	return sockfd;
}

int prepare_connection_udp(char *port, struct addrinfo *res) {

	struct addrinfo hints;
	bzero((char*) &hints, sizeof(hints));

	hints.ai_family = AF_INET6;	//	requesting a IPv6 local address will allow both IPv4 and IPv6 clients to use it
	hints.ai_socktype = SOCK_DGRAM;	//	SOCK_DGRAM for UDP
	hints.ai_flags = AI_PASSIVE;		// local address

	if (getaddrinfo(NULL, port, &hints, &res) != 0) {
		perror("failed to get local address");
		exit(EXIT_FAILURE);
	}

	int sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (sockfd == -1) {
		perror("failed to open socket");
		freeaddrinfo(res);
		exit(EXIT_FAILURE);
	}

	if (bind(sockfd, (struct sockaddr*) res->ai_addr, res->ai_addrlen) == -1) {
		perror("bind failed");
		close(sockfd);
		freeaddrinfo(res);
		exit(EXIT_FAILURE);
	}

	freeaddrinfo(res);
	listen(sockfd, SOMAXCONN);

	return sockfd;
}

void receive_from_udp(int sockfd, struct sockaddr_storage* client,
		socklen_t* adl, walls_t* walls, pthread_mutex_t* mux) {
	int res, i;
	char success[] = "Success";
	char fail[] = "Failed";
	char request_line[200], cliIPtext[200], cliPortText[200], wall_name[100];
	char* aux;

	res = recvfrom(sockfd, request_line, 200, 0, (struct sockaddr *) client,
			adl);
	request_line[res] = 0;

	if (!getnameinfo((struct sockaddr *) client, *adl, cliIPtext, 200,
			cliPortText, 200, NI_NUMERICHOST | NI_NUMERICSERV)) {
		aux = request_line;
		i = 0;
		while (*aux != '/') {
			wall_name[i] = *aux;
			aux++;
			i++;
		}
		aux++;
		wall_name[i] = 0;
		pthread_mutex_lock(mux);
		int index = find_wall(walls, wall_name);
		if (index == -1) {
			add_wall(walls, wall_name);
			index = find_wall(walls, wall_name);
		}
		add_message(walls->msg_walls + index, aux);
		pthread_mutex_unlock(mux);

		sendto(sockfd, success, res, 0, (struct sockaddr *) client, *adl);

	} else {

		sendto(sockfd, fail, res, 0, (struct sockaddr *) client, *adl);

	}
}

void* udp_server(void* a) {
	int sockfd;
	struct addrinfo *res = NULL;
	struct sockaddr_storage from;
	socklen_t adl = sizeof(from);
	arg* ar = (arg*) a;

	sockfd = prepare_connection_udp(ar->port, res);
	while (1) {
		receive_from_udp(sockfd, &from, &adl, ar->walls, ar->mux);
	}
	close(sockfd);
	free(ar->walls);
	pthread_exit((void*) NULL);
}

int main(int argc, char* argv[]) {

	struct addrinfo *res = NULL;
	int sockfd;
	int new_sock;
	struct sockaddr_storage from;
	socklen_t adl = sizeof(from);
	pthread_t thread;
	arg a;

	walls_t *walls = create_walls_list();
	a.walls = walls;
	a.port = argv[1];
	a.mux = &mux;

	if (argc != 2) {
		fprintf(stderr, "ERROR: local TCP port number missing\n");
		return 0;
	}

	if (pthread_create(&thread, NULL, udp_server, (void*) &a) != 0) {
		perror("error creating thread");
		exit(1);
	}

	sockfd = prepare_connection(argv[1], res);

	while (1) {
		new_sock = accept(sockfd, (struct sockaddr*) &from, &adl);
		process_http_request(sockfd, new_sock, walls, &mux);
	}

	pthread_mutex_destroy(&mux);
	close(sockfd);
	free(walls);
	return (0);
}
